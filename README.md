## Summary of the project

In recent years, social media data and especially Volunteered Geographic
Information (VGI) played a novel and increasingly important role in the
disaster management domain. Digital volunteers, who network virtually
and collaborate with each other, have a high potential to improve
decision-making processes by collecting, assessing, analysing and
verifying valuable, disaster-related information from social media such
as Twitter, Facebook, or YouTube. Digital volunteers have grown together
over time and formed specialised digital volunteer communities, like for
example the Humanitarian OpenStreetMap Team (HOT). Because of their open
community and rather low barriers for entering and exiting those
communities, these VGI communities can be characterised as "loosely
coupled communities". In the first phases, we investigated these
volunteers in more depth and conducted a final workshop with V&TCs and
decision makers from humanitarian organizations ([VGI and
Decision-Making in Disaster
Management](https://www.vgiscience.org/2020/01/22/vgi-and-decision-making-in-disaster-management.html)).

In parallel and in order to have better pre-established and functional
links between formal disaster management agencies and digital
volunteers, more and more disaster management agencies established
so-called Virtual Operations Support Teams (VOST). Frequently the team
members have practical experience in disaster management operations. Due
to these characteristics, the above-mentioned teams can be characterised
as "professionalised digital volunteer communities". The team itself
often works geographically dispersed at different locations and has a
liaison officer, who works directly in the emergency operation centre
either (EOC) or close-by. The volunteers create actionable information
for the disaster managers by detecting unusual events, creating crisis
maps or aggregating image analysis. To perform these tasks the team may
also collaborate with the more traditional, loosely coupled VGI
communities or other VOST-like teams. Due to the real-time character of
the operational work, the distributed decision-making of the
professionalised digital volunteers is typically performed in a
time-critical environment.

The overall goal of this research project was to gain a better knowledge
of the motivations and collaboration requirements between digital
volunteers and established EOCs, as well as the impact of information
products on the decision-making processes of emergency response
agencies. Based on the results of the first phase, the project addressed
several areas of interest:

-   motivational research related to the active participation of
    professionalised digital volunteer communities

-   organizational, procedural and technical requirements for the
    successful integration of the digital volunteer structures in formal
    response organisations

-   the improvement of time-critical and distributed decision-making
    processes of these teams at the interface of loosely coupled VGI
    communities and formal disaster management organisations

-   VGI, biases and decision-making in the context of disaster
    management

To conduct this research, a mixed-method approach using quantitative
surveys, interviews and workshops was applied.

## Motivational research

By using digital collaboration and analysis tools, digital volunteers
try to collect and map relevant disaster-related information from
publicly accessible sources, process it and make it available to
disaster management decision-makers. In doing so, the volunteers are
dislocated from the actual disaster area, as they are mobile and do not
need to be close to the site. Over time, numerous digital volunteer
organizations, so-called Volunteer & Technical Communities (V&TCs), have
been founded and professionalized. In the past years, the academic
discussion of this topic has mainly focused on technical and
organizational aspects. Only few papers deal with organization-specific
reasons for and barriers to participation of digital volunteers in
disaster management. [The
paper](https://dl.gi.de/handle/20.500.12116/33547) contains a detailed
analysis of motivational factors of operationally active digital
volunteers. In a cross-organizational online survey, possible motives,
individual organizational commitment, and potential incentive options
were analysed. In addition, two experienced digital team leaders of
V&TCs were interviewed in guided expert interviews about methods and
measures for increasing motivation and organizational commitment. Based
on the findings, explanatory patterns for the motivational factors of
digital volunteers can be derived on the one hand; on the other hand,
measures that are useful and create identification can be identified.
Parts of this paper were presented on January 14, 2023, at the
conference organized by the Federal Office of Civil Protection and
Disaster Assistance at the World Conference Center in Bonn, Germany.

![](_images/media/image1.jpeg){:style="float:left; padding:0 4em 2em 0; height:450px"}
[![](_images/media/image2.jpeg){:style="float:right; padding:0 0 2em 4em; height:450px"}][1]

[1]: https://dl.gi.de/handle/20.500.12116/33547

## Research in the context of Virtual Operations Support Teams (VOST)

The amount and variety of available VGI in disaster management has been
revolutionized with the emergence of social media. Today, crisis
responders can find huge amounts of timely eyewitness accounts, on-site
photos and videos, crowd-behavioural information and situation awareness
insights by analysing these platforms. Of course, these novel data
channels can only be utilized if the experts find ways to cope with
their large, noisy, and uncertain nature in order to turn that
information overload into actionable insights.

To address that challenge, a new form of collaborative digital
volunteering effort, called the Virtual Operations Support Team (VOST),
has recently emerged in various countries around the globe. A VOST
consists of members who are not only experts in social media analytics,
web technologies, and information processing, but who also have
significant experience and background in disaster management and crisis
response. The team usually works dislocated from the actual site.
However, it is often tightly connected or even integrated with the
traditional disaster management institutions and sends technical
advisors and liaison officers to the emergency operation centre. VOST
often utilize various data mining and data visualization tools in order
to monitor the huge real-time streams of data, verify potentially
relevant entities, and represent insights in the form of interactive
crisis maps and interactive reports.

To understand the organizational, procedural and technical requirements
for the successful integration of VOST in formal response organisations,
a case study was conducted in a SPP-collaboration with the project
"VA4VGI". The paper is published under the title "[VOST: A case study in
voluntary digital participation for collaborative emergency
management](https://www.sciencedirect.com/science/article/abs/pii/S0306457319302316?via%3Dihub)"
in the journal "Information Processing and Management".

In a further case study, the data collected during the 2021 flood in
Wuppertal, Germany by 22 VOST analysts was processed and analysed. The
paper is published under the title "[Social Media Analytics by Virtual
Operations Support Teams in disaster management: Situational awareness
and actionable information for
decision-makers](https://www.frontiersin.org/articles/10.3389/feart.2022.941803/full)"
in the journal "Frontiers in Earth Science". It was found that
information from eight social media platforms could be classified into
23 distinct categories. The analysts' prioritizations indicate
differences in the formats of information and platforms.
Disaster-related posts that pose a threat to the affected population's
health and safety (e.g., requests for help or false information) were
more commonly prioritized than other posts. Image heavy content was also
rated higher than text-heavy data. A subsequent survey of EOC
decision-makers examined the impact of VOST information on situational
awareness during this flood. It also asked how actionable information
impacted decisions. It was found that VOST information contributes to
expanded situational awareness of decision-makers and ensures
people-centred risk and crisis communication. Based on the results from
this case study, the paper discusses the need for future research in the
area of integrating VOST analysts in decision-making processes in the
field of time-critical disaster management.

[![](_images/media/image3.jpeg){:style="float:left;padding:0 4em 2em; height:490px"}][2]
[![](_images/media/image4.jpeg){:style="float:right;padding:0 4em 2em; height:490px"}][3]

[2]: https://www.frontiersin.org/articles/10.3389/feart.2022.941803/full
[3]: https://www.sciencedirect.com/science/article/abs/pii/S0306457319302316?via%3Dihub


## Data and Cognitive Bias in Crisis Information Management

To understand the interplay of data and cognitive bias in the context of
using VGI in disaster management in more detail, this project and the
Delft University of Technology jointly conducted the experimental
workshop \"[Volunteered Geographic Information (VGI) and Decision-Making
in Disaster
Management](https://www.vgiscience.org/2020/01/22/vgi-and-decision-making-in-disaster-management.html)\"
in 2020. The goal of this experimental workshop was to gain insights
into and a deeper understanding of how virtual information flows and
products can be designed to meet the requirements of decision-makers to
therefore increase the use of information, which is provided virtually.
Furthermore, it was analysed, how data and cognitive bias influence
analysing VGI by digital volunteers and decision-making in disaster
management.

![](_images/media/image5.jpeg){:style="float:left;padding:0 2em 2em 0; height:400px"}
[![](_images/media/image6.jpeg){:style="float:right;padding:0 0 2em 2em; height:400px"}][4]

[4]: https://link.springer.com/article/10.1007/s10796-022-10241-0


The workshop used a scenario-based setup, where an
epidemic disaster scenario served as the overarching case study. The
data and information was created based on real data from an epidemic and
were manipulated in such a way that no direct conclusion about the
disease and affected places was possible. The sparse database was thus
mapped realistically, whereas the countries in the scenario were
fictitious. The findings where published under the title "[On the
Interplay of Data and Cognitive Bias in Crisis Information Management:
An Exploratory Study on Epidemic
Response](https://link.springer.com/article/10.1007/s10796-022-10241-0)"
in the journal "Information Systems Frontiers".


## Privacy-aware Social Media Data Processing in Disaster Management

[![](_images/media/image7.jpeg){:style="float:right;padding:0 0 2em 2em; height:600px"}][5]

[5]: https://www.mdpi.com/2220-9964/9/12/709

Another SPP internal collaboration with the projects "Privacy Aware" and
"VA4VGI" has addressed as a Young Research Group the question of how
[VOST can process social media data with privacy-aware methods and
algorithms](https://www.mdpi.com/2220-9964/9/12/709). A focus group
discussion with VOST members were developed and conducted, where
challenges and opportunities of working with HLL were identified and
compared with conventional techniques. Findings showed that deploying
HLL in the data acquisition process of VOST operations would not
distract their data analysis process. Instead, several benefits, such as
improved working with huge datasets, may contribute to a more widespread
use and adoption of the presented technique, which provides a basis for
a better integration of privacy considerations in disaster management.


## Practical implications

In addition to the academic contribution,
numerous practical implications have been achieved and implemented from
the project. Under the editorial of Ramian Fathi and Michael Lülf,
"Kohlhammer Verlag" published a practice-oriented collection of articles
in December 2022. In 23 articles, authors from science and operational
practice discuss various aspects of the use of social media in emergency
response. Differentiated in four categories, the contributions deal with
fundamentals & concepts, methods and strategies of communication through
social media by emergency responders, social media analytics and
psychosocial emergency care in social media. The book can be purchased
at the following link:
<https://shop.kohlhammer.de/soziale-medien-in-der-gefahrenabwehr-34913.html#147=22>

[![](_images/media/image8.jpeg){:style="float:left;padding:0 2em 2em 0; height:600px"}][6]
[![](_images/media/image9.PNG){:style="float:right;padding:0 0 2em 2em; height:600px"}][7]

[6]: https://shop.kohlhammer.de/soziale-medien-in-der-gefahrenabwehr-34913.html#147=22
[7]: https://shop.kohlhammer.de/integration-von-spontanhelfenden-durch-interorganisationale-zusammenarbeit-978-3-00-424809-8.html

In addition, numerous (non-reviewed)
publications have been published in disaster management journals (see
publication list below). A [paper written in collaboration with the
Wuppertal Fire
Department](https://shop.kohlhammer.de/integration-von-spontanhelfenden-durch-interorganisationale-zusammenarbeit-978-3-00-424809-8.html),
among others, discussed the use of a VOST during the 2021 flood.

## Interviews

In addition, a number of media interviews were conducted to report on
the project and the use of social media in disaster management.

1.  Interview (2023): „Katastrophen voraus" im Magazin für
    Sicherheitspolitik \"loyal\".
    <https://www.reservistenverband.de/magazin-loyal/katastrophen-voraus/>

2.  Interview (2023): „Nach 2021: Der richtige Umgang mit
    Hochwasserwarnungen".
    <https://www.wp.de/region/sauer-und-siegerland/nach-2021-der-richtige-umgang-mit-hochwasserwarnungen-id237397301.html>

3.  Interview (2021): Resiliente Frühwarnsysteme tun not. In: VDI
    nachrichten 75 (33-34), S. 17. DOI:
    [doi.org/10.51202/0042-1758-2021-33-34-17](https://doi.org/10.51202/0042-1758-2021-33-34-17)

4.  Interview (2021): Katastrophen-Warnsystem.
    [www.riffreporter.de/de/umwelt/katastrophen-warnsystem-smartphone-soziale-medien](https://www.riffreporter.de/de/umwelt/katastrophen-warnsystem-smartphone-soziale-medien).
    Hg. v. Riffreporter.

5.  Interview (2021): [Soziale Medien in
    Katastrophen](https://youtu.be/JHtcPtGDAmE). Forschungsmagazin
    BUW.OUTPUT.

6.  Interview (2019). [Interview im Feuerwehr-Magazin: Soziale Medien
    richtig
    nutzen.](https://www.feuerwehrmagazin.de/wissen/spontanhelfer-beim-feuerwehr-einsatz-einbinden-oder-ignorieren-83597)
    In Feuerwehr-Magazin (5/2019). Bremen.

## Publications

1.  Editorial

    a.  Michael Lülf & Ramian Fathi (eds.) (2022): [Soziale Medien in der
        Gefahrenabwehr](https://shop.kohlhammer.de/soziale-medien-in-der-gefahrenabwehr-34913.html).
        Stuttgart. Kohlhammer Verlag. ISBN: 978-3-17-034913-1

2.  Journal-Paper (peer-reviewed)

    b.  Fathi, R. & Fiedrich, F. (2022). [Social Media Analytics by Virtual
        Operations Support Teams in Disaster Management: Situational
        Awareness and Actionable Information for
        Decision-Makers.](https://www.frontiersin.org/articles/10.3389/feart.2022.941803/abstract) Frontiers
        in Earth Science. DOI: <https://doi.org/10.3389/feart.2022.941803>

    c.  Fathi, R., Thom, D., Koch, S., Ertl, T., & Fiedrich, F. (2020).
        [VOST: A case study in voluntary digital participation for
        collaborative emergency
        management](https://www.sciencedirect.com/science/article/abs/pii/S0306457319302316?via%3Dihub).
        Information Processing & Management, 57(1). DOI:
        [doi.org/10.1016/j.ipm.2019.102174](https://doi.org/10.1016/j.ipm.2019.102174)

    d.  Paulus, D., Fathi, R., Fiedrich, F., van de Walle, B., and Comes, T.
        (2022). [On the Interplay of Data and Cognitive Bias in Crisis
        Information Management. An Exploratory Study on Epidemic
        Response](https://link.springer.com/article/10.1007/s10796-022-10241-0).
        Information Systems Frontiers. DOI:
        <https://doi.org/10.1007/s10796-022-10241-0>

    e.  Löchner, M., Fathi, R., Schmid, D., Dunkel, A., Burghardt, D.,
        Fiedrich, F., Koch, S. (2020). [Case Study on Privacy-aware Social
        Media Data Processing in Disaster
        Management](https://www.mdpi.com/2220-9964/9/12/709). International
        Journal of Geo-Information (ISPRS)., 9, 709. DOI:
        [doi.org/10.3390/ijgi9120709](https://doi.org/10.3390/ijgi9120709) 

3.  Conference-Paper (peer-reviewed)

    f.  Fathi, R. & Fiedrich, F., (2020). [Digital Freiwillige in der
        Katastrophenhilfe - Motivationsfaktoren und Herausforderungen der
        Partizipation](https://dl.gi.de/handle/20.500.12116/33547). In:
        Hansen, C., Nürnberger, A. & Preim, B. (Hrsg.), Mensch und Computer
        2020 - Workshopband. Bonn: Gesellschaft für Informatik e.V. DOI:
        [dx.doi.org/10.18420/muc2020-ws117-406](https://dx.doi.org/10.18420/muc2020-ws117-406)

    g.  Sonntag, F., Fathi, R. & Fiedrich, F., (2021). [Digitale
        Lageerkundung bei Großveranstaltungen: Erweiterung des Lagebildes
        durch Erkenntnisse aus sozialen
        Medien](https://dl.gi.de/handle/20.500.12116/37411). In: Wienrich,
        C., Wintersberger, P. & Weyers, B. (Hrsg.), Mensch und Computer
        2021 - Workshopband. Bonn: Gesellschaft für Informatik e.V.. DOI:
        [dx.doi.org/10.18420/muc2021-mci-ws08-262](https://dx.doi.org/10.18420/muc2021-mci-ws08-262)

    h.  Fathi, R., Fiedrich, F. (2017). [Motivation and Participation of
        Digital Volunteer Communities in Humanitarian
        Assistance.](http://idl.iscram.org/files/tinacomes/2017/1440_TinaComes_etal2017.pdf)
        Conference: Proceedings of the 14th International Conference on
        Information Systems for Crisis Response And Management (pp.
        412-419). Albi, France. ISSN: 2411-3387

4.  Book-Chapter

    h.  Sonntag, F., Fathi, R. & Fiedrich, F. (2022): [KI-gestützte
        Lagebilder in der Pandemiebekämpfung -- Möglichkeiten und Grenzen in
        einer digitalisierten
        Gesellschaft](https://shop.kohlhammer.de/resilienz-und-pandemie-39930.html).
        In: Karsten, A. & Voßschmidt, S. (Eds), Resilienz und Pandemie --
        Handlungsempfehlungen anhand von Erfahrungen mit COVID-19.
        Stuttgart: Kohlhammer Verlag. ISBN: 978-3-17-039930-3

    i.  Fiedrich, F. & Fathi, R. (2021). Humanitäre Hilfe und Konzepte der
        digitalen Hilfeleistung. In: Reuter, C. (eds) Sicherheitskritische
        Mensch-Computer-Interaktion. Springer Vieweg, Wiesbaden.
        <https://doi.org/10.1007/978-3-658-32795-8_25>

    j.  Tackenberg, B., Fathi, R., Schütte, P., & Fiedrich, F. (2020).
        Resilienz durch Partizipation: Herausforderungen auf
        zivilgesellschaftlicher und organisationaler Ebene. In S. Voßschmidt
        & A. Karsten (Eds.), [Resilienz und kritische Infrastrukturen.
        Aufrechterhaltung von Versorgungstrukturen im
        Krisenfall.](https://www.kohlhammer.de/wms/instances/KOB/appDE/nav_product.php?product=978-3-17-035433-3&world=BOOKS)
        Stuttgart: Kohlhammer. ISBN: 978-3-17-035433-3

    k.  Fathi R., Brixy AM., Fiedrich F. (2019) Desinformationen und
        Fake-News in der Lage: Virtual Operations Support Team (VOST) und
        Digital Volunteers im Einsatz. In: Lange HJ., Wendekamm M. (eds)
        Postfaktische Sicherheitspolitik. Studien zur Inneren Sicherheit,
        vol 23. Springer VS, Wiesbaden. DOI:
        [doi.org/10.1007/978-3-658-27281-4_11](https://doi.org/10.1007/978-3-658-27281-4_11)

    l.  Fathi R., Martini S., Fiedrich F. (2019) Eine veränderte
        Kommunikationskultur: Risiko- und Krisenkommunikation und Monitoring
        mittels sozialer Medien bei Großveranstaltungen. In: Groneberg C.
        (eds) Veranstaltungskommunikation. Springer VS, Wiesbaden. DOI:
        [doi.org/10.1007/978-3-658-11725-2_7](https://doi.org/10.1007/978-3-658-11725-2_7)

    m.  Fathi, R., & Fiedrich, F. (2019). [Exkurs: Das Virtual Operations
        Support Team
        (VOST).](https://www.drk.de/fileadmin/user_upload/Forschung/schriftenreihe/band_5/Webversion_Schriftenreihe_Band_5_Teil_2.pdf)
        In Deutsches Rotes Kreuz (Ed.), Schriftenreihe der Forschung: Web
        2.0 und Soziale Medien im Bevölkerungsschutz. Teil 2: Die Rolle von
        Digital Volunteers bei der Bewältigung von Krisen- und
        Katastrophenlagen. (pp. 23-25). Berlin.

    n.  Fiedrich, F. & Fathi, R. (2018). [Humanitäre Hilfe und Konzepte der
        digitalen
        Hilfeleistung.](https://doi.org/10.1007/978-3-658-19523-6_25)
        Sicherheitskritische Mensch-Computer-Interaktion: Interaktive
        Technologien und Soziale Medien im Krisen- und
        Sicherheitsmanagement, pp. 509-528. Springer Vieweg. DOI:
        <https://doi.org/10.1007/978-3-658-19523-6_25>

    o.  Fathi, R., Tonn, C., Schulte, Y., Spang, A., Gründler, D., Kletti,
        M., Fiedrich, F., Fekete, A., Martini, S. (2016). [„Untersuchung der
        Motivationsfaktoren von
        Spontanhelfern".](https://www.rotkreuzshop.de/service/de/shop/verlag/bereitschaften/die-rolle-von-ungebundenen-helferinnen-bei-schadensereignissen-3-ve-10-stueck/?card=1129)
        Schriften der Sicherheitsforschung -- Band 1. Deutsches Rotes Kreuz:
        Die Rolle von ungebundenen HelferInnen bei der Bewältigung von
        Schadensereignissen. Teil 3: Handlungs- und Umsetzungsempfehlungen
        für den Einsatz ungebundener HelferInnen. Berlin. 

5.  Journal-Paper (non-reviewed)

    p.  Bier, M., Fathi, R., Fiedrich, F., Kahl, A., Peschelt, H. &
        Schlubeck, B. (2022): [Integration von Spontanhelfenden durch
        interorganisationale Zusammenarbeit - Erkenntnisse aus der
        Hochwasserlage 2021 in
        Wuppertal](https://shop.kohlhammer.de/integration-von-spontanhelfenden-durch-interorganisationale-zusammenarbeit-978-3-00-424809-8.html).
        In: BRANDSCHUTZ, 12/2022, Kohlhammer Verlag. ISSN 0006-9094

    q.  Sonntag, F., Fathi, R. & Emrich, C. (2022): [VOST: die Entwicklung
        der Einheit und ihr Einsatz beim
        G7-Gipfel](https://shop.kohlhammer.de/vost-die-entwicklung-der-einheit-und-ihr-einsatz-beim-g7-gipfel-978-3-00-424784-8.html).
        In: BRANDSCHUTZ, 10/2022, Kohlhammer Verlag. ISSN 0006-9094

    r.  Fathi, Ramian (2021): Soziale Medien in Katastrophen.
        [www.buw-output.uni-wuppertal.de/fileadmin/buw-output/Ausgabe_25/Output_25-02-2021_DS.pdf](https://www.buw-output.uni-wuppertal.de/fileadmin/buw-output/Ausgabe_25/Output_25-02-2021_DS.pdf).
        Herausforderungen und Lösungsansätze in einer hochvernetzten
        Gesellschaft. In: BUW Output (25), S. 24--30.

    s.  Fathi, R., Hugenbusch, D. (2021). [VOST: Digitale
        Einsatzunterstützung in
        Deutschland](https://crisis-prevention.de/katastrophenschutz/vost-digitale-einsatzunterstuetzung-in-deutschland.html).
        Crisis Prevention, 1/2021. Beta Verlag. ISSN: 2198-0527

    t.  Tackenberg, B., Fathi, R., Lukas, T. (2020) [Partizipation im
        digitalen Zeitalter - Sozialer Zusammenhalt in der Corona-Krise.
        Stadtpunkte
        Thema](https://www.hag-gesundheit.de/fileadmin/hag/data/Medien/Stadtpunkte/1StadtpunkteThema-1-2020.pdf)
        , 1/2020, pp. 8-9. Hamburgische Arbeitsgemeinschaft für
        Gesundheitsförderung e.V. (HAG). ISSN: 1860-7276

    u.  Fathi, R., Kleinebrahn, A., Voßschmidt, S., Polan, F., Karsten, A.
        (2020). [Social Media und die
        Corona-Pandemie](https://fk07-ls.cms.uni-wuppertal.de/). In:
        Notfallvorsorge, 03/2020, pp. 16-19. Walhalla-Verlag. ISSN:
        0948-7913

    v.  Fathi, R.; Schulte, Y.; Fiedrich, F. (2019). [Kritische
        Infrastrukturen und grenzüberschreitende Herausforderungen einer
        vernetzten
        Gesellschaft](https://www.researchgate.net/publication/331951513_Kritische_Infrastrukturen_und_grenzuberschreitende_Herausforderungen_einer_vernetzten_Gesellschaft).
        Notfallvorsorge, 1/2019, pp. 07-13. Walhalla-Verlag. ISSN: 0948-7913

    w.  Fathi, R.; Schulte, Y.; Schütte P.; Tondorf V.; Fiedrich, F. (2018).
        [Lageinformationen aus den sozialen Netzwerken: Virtual Operations
        Support Teams (VOST) international im
        Einsatz.](https://www.researchgate.net/publication/325781437_Lageinformationen_aus_den_sozialen_Netzwerken_Virtual_Operations_Support_Teams_VOST_international_im_Einsatz)
        Notfallvorsorge, 2/2018, pp. 01-09. Walhalla-Verlag. ISSN:
        0948-7913.

    x.  Fathi, R., Kleinebrahn, A., Schulte, Y., Martini, S. (2017).
        [Desinformationen in der Lage -- oder die Suche nach dem Koch der
        Gerüchteküche.](https://www.researchgate.net/publication/322267348_Desinformationen_in_der_Lage_-_oder_die_Suche_nach_dem_Koch_der_Geruchtekuche)
        Crisis Prevention, 4/2017, pp. 38-41. Beta Verlag. ISSN: 2198-0527

    y.  Fathi, R., Polan, F., Fiedrich, F. (2017). [Digitale Hilfeleistung
        und das Digital Humanitarian
        Network.](https://www.researchgate.net/publication/319417883_Digitale_Hilfeleistung_und_das_Digital_Humanitarian_Network)
        Notfallvorsorge, 3/2017, pp. 4-10. Walhalla-Verlag. ISBN:
        978-3-8029-4875-6

    z.  Fathi, R., Rummeny, D., Fiedrich, F. (2017). [Organisation von
        Spontanhelfern am Beispiel des Starkregenereignisses vom 28.07.2014
        in
        Münster.](https://www.researchgate.net/publication/319418283_Organisation_von_Spontanhelfern_am_Beispiel_des_Starkregenereignisses_vom_28072014_in_Munster)
        Notfallvorsorge, 2/2017, pp. 27-34. Walhalla-Verlag. ISBN:
        978-3-8029-4845-9

    aa.  Fathi, R., Martini, S., Kleinebrahn, A., Voßschmidt, S. (2017).
        Fathi, R., Rummeny, D., Fiedrich, F. (2017). [Spontanhelfer im
        Bevölkerungsschutz: Rahmenempfehlungen für den Einsatz von Social
        Media.](https://www.researchgate.net/publication/322267232_Spontanhelfer_im_Bevolkerungsschutz_Rahmenempfehlungen_fur_den_Einsatz_von_Social_Media)
        Notfallvorsorge, 1/2017, pp. 08-14. Walhalla-Verlag.
        ISBN:978-3-8029-4844-2
